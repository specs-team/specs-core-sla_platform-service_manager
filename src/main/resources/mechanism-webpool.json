{
  "security_mechanism_id": "SM1",
  "security_mechanism_name": "webpool",
  "sm_description": "This security mechanisms aims at offering web servers and the capabilities of surviving to security incidents involving a web server, by implementing proper strategies aimed at preserving business continuity, achieved through redundancy and/or diversity.",
  "security_capabilities": [
    "web_resilience"
  ],
  "enforceable_metrics": [
    "level_of_redundancy_m1",
    "level_of_diversity_m2"
  ],
  "monitorable_metrics": [
    "level_of_redundancy_m1",
    "level_of_diversity_m2"
  ],
  "measurements": [
    {
      "msr_id": "number_of_servers_wp_msr1",
      "msr_description": "Number of responsive web servers.",
      "frequency": "1h",
      "metrics": [
        "level_of_redundancy_m1"
      ],
      "monitoring_event": {
        "event_id": "number_of_servers_too_low_wp_e1",
        "event_description": "The number of responsive web servers is too low.",
        "event_type": "VIOLATION",
        "condition": {
          "operator": "less",
          "threshold": "level_of_redundancy_m1"
        }
      }
    },
    {
      "msr_id": "diversity_level_wp_msr2",
      "msr_description": "Number of web server types active.",
      "frequency": "1h",
      "metrics": [
        "level_of_diversity_m2"
      ],
      "monitoring_event": {
        "event_id": "diversity_level_too_low_wp_e2",
        "event_description": "The number of web server types is too low.",
        "event_type": "VIOLATION",
        "condition": {
          "operator": "less",
          "threshold": "level_of_diversity_m2"
        }
      }
    }
  ],
  "metadata": {
    "components": [
      {
        "component_name": "haproxy",
        "component_type": "balancer",
        "recipe": "webpool",
        "cookbook": "wp_r5",
        "implementation_step": 1,
        "pool_seq_num": 1,
        "pool_id": "webpool",
        "vm_requirement": {
          "hardware": "t1_micro",
          "usage": "100",
          "acquire_public_ip": "true",
          "private_ips_count": 1,
          "firewall": {
            "incoming": {
              "source_ips": [
                "0.0.0.0/0"
              ],
              "source_nodes": [
                "string"
              ],
              "interface": "public",
              "proto": [
                "TCP"
              ],
              "port_list": [
                "22",
                "80",
                "443"
              ]
            },
            "outcoming": {
              "destination_ips": [
                "0.0.0.0/0"
              ],
              "destination_nodes": [],
              "interface": "public,private:1",
              "proto": [
                "TCP"
              ],
              "port_list": [
                "*"
              ]
            }
          }
        }
      },
      {
        "component_name": "apache",
        "component_type": "web_server",
        "recipe": "webpool",
        "cookbook": "wp_r6",
        "implementation_step": 1,
        "pool_seq_num": 1,
        "pool_id": "webpool",
        "vm_requirement": {
          "hardware": "t1_micro",
          "usage": "50",
          "acquire_public_ip": "false",
          "private_ips_count": 1,
          "firewall": {
            "incoming": {
              "source_ips": [],
              "source_nodes": [
                "wp_haproxy"
              ],
              "interface": "private:1",
              "proto": [
                "TCP"
              ],
              "port_list": [
                "22",
                "80",
                "443"
              ]
            },
            "outcoming": {
              "destination_ips": [],
              "destination_nodes": [
                "wp_haproxy"
              ],
              "interface": "private:1",
              "proto": [
                "TCP"
              ],
              "port_list": [
                "*"
              ]
            }
          }
        }
      },
      {
        "component_name": "nginx",
        "component_type": "web_server",
        "recipe": "webpool",
        "cookbook": "wp_r7",
        "implementation_step": 1,
        "pool_seq_num": 1,
        "pool_id": "webpool",
        "vm_requirement": {
          "hardware": "t1_micro",
          "usage": "50",
          "acquire_public_ip": "false",
          "private_ips_count": 1,
          "firewall": {
            "incoming": {
              "source_ips": [],
              "source_nodes": [
                "wp_haproxy"
              ],
              "interface": "private:1",
              "proto": [
                "TCP"
              ],
              "port_list": [
                "22",
                "80",
                "443"
              ]
            },
            "outcoming": {
              "destination_ips": [],
              "destination_nodes": [
                "wp_haproxy"
              ],
              "interface": "private:1",
              "proto": [
                "TCP"
              ],
              "port_list": [
                "*"
              ]
            }
          }
        }
      }
    ],
    "constraints": [
      {
        "ctype": "SC1a",
        "arg1": [
          "wp_haproxy"
        ],
        "arg2": [
          "wp_apache",
          "wp_nginx"
        ]
      },
      {
        "ctype": "SC1a",
        "arg1": [
          "wp_apache"
        ],
        "arg2": [
          "wp_nginx"
        ]
      },
      {
        "ctype": "SC2a_1",
        "arg1": [
          "wp_haproxy"
        ],
        "op": "=",
        "n1": "1"
      },
      {
        "ctype": "SC2b_2",
        "arg1": [
          "wp_apache",
          "wp_nginx"
        ],
        "op": ">=",
        "n1": "level_of_redundancy_m1"
      },
      {
        "ctype": "SC3",
        "n1": "level_of_redundancy_m1+1"
      }
    ]
  },
  "remediation": {
    "remediation_actions": [
      {
        "name": "wp_a1",
        "action_description": "Check if the number of responsive web servers is greater than or equal to level_of_redundancy_m1 value.",
        "recipes": [
          "wp_r1"
        ]
      },
      {
        "name": "wp_a2",
        "action_description": "Restart unresponsive web server and check if number of responsive web servers is greater than or equal to level_of_redundancy_m1 value.",
        "recipes": [
          "wp_r2",
          "wp_r1"
        ]
      },
      {
        "name": "wp_a3",
        "action_description": "Replace unresponsive web server and check if number of responsive web servers is greater than or equal to level_of_redundancy_m1 value.",
        "recipes": [
          "wp_r3",
          "wp_r1"
        ]
      },
      {
        "name": "wp_a4",
        "action_description": "Check if the number of responsive web server types is greater than or equal to level_of_diversity_m2 value.",
        "recipes": [
          "wp_r4"
        ]
      },
      {
        "name": "wp_a5",
        "action_description": "Restart unresponsive web server and check if the number of responsive web server types is greater than or equal to level_of_diversity_m2 value.",
        "recipes": [
          "wp_r2",
          "wp_r4"
        ]
      },
      {
        "name": "wp_a6",
        "action_description": "Replace unresponsive web server and check if the number of responsive web server types is greater than or equal to level_of_diversity_m2 value.",
        "recipes": [
          "wp_r3",
          "wp_r4"
        ]
      }
    ],
    "remediation_flow": [
      {
        "name": "redundancy_too_low_wp_e1",
        "action_id": "wp_a1",
        "yes_action": "observe",
        "no_action": {
          "action_id": "wp_a2",
          "yes_action": "observe",
          "no_action": {
            "action_id": "wp_a3",
            "yes_action": "observe",
            "no_action": "notify"
          }
        }
      },
      {
        "name": "diversity_too_low_wp_e2",
        "action_id": "wp_a4",
        "yes_action": "observe",
        "no_action": {
          "action_id": "wp_a5",
          "yes_action": "observe",
          "no_action": {
            "action_id": "wp_a6",
            "yes_action": "observe",
            "no_action": "notify"
          }
        }
      }
    ]
  },
  "chef_recipes": [
    {
      "name": "wp_r1",
      "recipe_description": "Take measurement wp-msr1 and label the event as remediation-event.",
      "associated_metrics": [],
      "associated_measurements": [],
      "dependent_components": [
        "wp_haproxy"
      ]
    },
    {
      "name": "wp_r2",
      "recipe_description": "Restart unresponsive servers.",
      "associated_metrics": [],
      "associated_measurements": [],
      "dependent_components": [
        "wp_haproxy",
        "wp_apache",
        "wp_nginx"
      ]
    },
    {
      "name": "wp_r3",
      "recipe_description": "Replace unresponsive servers.",
      "associated_metrics": [
        "specs:level_of_redundancy:M1",
        "specs:level_of_diversity:M2"
      ],
      "associated_measurements": [],
      "dependent_components": [
        "wp_haproxy",
        "wp_apache",
        "wp_nginx"
      ]
    },
    {
      "name": "wp_r4",
      "recipe_description": "Take measurement wp-msr2 and label the event as remediation-event.",
      "associated_metrics": [],
      "associated_measurements": [],
      "dependent_components": [
        "wp_haproxy"
      ]
    },
    {
      "name": "wp_r5",
      "recipe_description": "Install HAProxy.",
      "associated_metrics": [],
      "associated_measurements": [
        "specs:level_of_redundancy:M1",
        "specs:level_of_diversity:M2"
      ],
      "dependent_components": [
        "wp_apache",
        "wp_nginx"
      ]
    },
    {
      "name": "wp_r6",
      "recipe_description": "Install Apache.",
      "associated_metrics": [],
      "associated_measurements": [],
      "dependent_components": [
        "wp_haproxy"
      ]
    },
    {
      "name": "wp_r7",
      "recipe_description": "Install Nginx.",
      "associated_metrics": [],
      "associated_measurements": [],
      "dependent_components": [
        "wp_haproxy"
      ]
    }
  ]
}