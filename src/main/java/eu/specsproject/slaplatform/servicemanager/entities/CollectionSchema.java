/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
@author  Pasquale De Rosa p.derosa@teamandroid.it
 */

package eu.specsproject.slaplatform.servicemanager.entities;

import java.util.List;

public class CollectionSchema {
    private String resource;
    private Integer total;
    private Integer members;
    private List<String> itemList;
    
    public CollectionSchema(){
        //Zero arguments constructor
    }
    
    public CollectionSchema(String resource, Integer total, Integer members, List<String> itemList){
        this.resource = resource;
        this.total = total;
        this.members = members;
        this.setItemList(itemList);
    }

    public String getResource() {
        return resource;
    }
    public void setResource(String resource) {
        this.resource = resource;
    }
    public Integer getTotal() {
        return total;
    }
    public void setTotal(Integer total) {
        this.total = total;
    }
    public Integer getMembers() {
        return members;
    }
    public void setItems(Integer members) {
        this.members = members;
    }
    public List<String> getItemList() {
        return itemList;
    }

    public void setItemList(List<String> itemList) {
        this.itemList = itemList;
    }

}
