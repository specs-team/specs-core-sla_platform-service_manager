/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
@author  Pasquale De Rosa p.derosa@teamandroid.it
 */

package eu.specsproject.slaplatform.servicemanager.entities;

import java.io.Serializable;

public class SecurityMechanismDocument implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = -3807753266137195561L;
    private String securityMechanismJsonDocument;
    
    public SecurityMechanismDocument(){
        //Zero arguments constructor
    }

    public SecurityMechanismDocument(String doc){
        this.securityMechanismJsonDocument = doc;
    }

    public String getSecurityMechanismJsonDocument() {
        return securityMechanismJsonDocument;
    }

    public void setSecurityMechanismJsonDocument(
            String securityMechanismJsonDocument) {
        this.securityMechanismJsonDocument = securityMechanismJsonDocument;
    }

}
