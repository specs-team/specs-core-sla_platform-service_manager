/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
@author  Pasquale De Rosa p.derosa@teamandroid.it
 */

package eu.specsproject.slaplatform.servicemanager.internal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.specs.datamodel.enforcement.SecurityCapability;
import eu.specs.datamodel.enforcement.SecurityMechanism;
import eu.specsproject.slaplatform.servicemanager.PersistenceEntityManager;
import eu.specsproject.slaplatform.servicemanager.entities.SecurityCapabilityIdentifier;
import eu.specsproject.slaplatform.servicemanager.entities.SecurityMechanismIdentifier;

/**
 * JPA SQL implementation.
 * This code is not intended to be part of the public API.
 * 
 * @author Pasquale De Rosa SPECS - CeRICT
 *
 */
public final class EUServiceManagerSQLJPA extends EUServiceManagerAbstractImpl {

    private EntityManagerFactory emFactory;

    public EUServiceManagerSQLJPA (boolean isTest){
        emFactory = PersistenceEntityManager.getInstance().getEntityManagerFactory(isTest);
    }


    //SECURITY MECHANISMS PERSISTENCE MANAGE
    @Override
    void persistenceCreateSM(SecurityMechanism securityMechanism) {
        EntityManager em =  emFactory.createEntityManager();
        EntityTransaction t = em.getTransaction();
        t.begin();
        em.persist(securityMechanism);
        t.commit();
        em.close();
    }

    @Override
    SecurityMechanism persistenceGetSMByID(SecurityMechanismIdentifier id){
        if (id==null)
            throw new IllegalArgumentException("SecurityMechanism_Identifier cannot be null");
        String iD = id.getId();
        EntityManager em =  emFactory.createEntityManager();

        SecurityMechanism securityMechanism = em.find(SecurityMechanism.class,iD);

        if (securityMechanism==null)
            throw new IllegalArgumentException("SecurityMechanism_Identifier is not valid");

        em.close();
        return securityMechanism;
    }

    @Override
    List<SecurityMechanismIdentifier> persistenceSearchSM(List<String> capabilitiesId, List<String> metricsId, int start, int stop){

        EntityManager em =  emFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SecurityMechanism> cq = cb.createQuery(SecurityMechanism.class);

        List<SecurityMechanismIdentifier> result = new ArrayList<SecurityMechanismIdentifier>();
        List<SecurityMechanism> list;

        if(stop != -1){
            list = em.createQuery(cq).setFirstResult(start).setMaxResults(stop-start).getResultList();
        }else{
            list = em.createQuery(cq).setFirstResult(start).getResultList();
        }

        if(capabilitiesId != null){
            result = checkMultipleCapabilities(capabilitiesId, list);
            em.close();
            return result;
        }else if(metricsId != null){
            result = checkMultipleMetricsId(metricsId, list);
            em.close();
            return result;
        }
        
        for (SecurityMechanism securityMechanism : list){
            result.add(new SecurityMechanismIdentifier(String.valueOf(securityMechanism.getId())));
        }
        
        em.close();
        return result;
    }

    private List<SecurityMechanismIdentifier> checkMultipleCapabilities (List<String> capabilitiesId, List<SecurityMechanism> securityMechanisms){
        List<SecurityMechanismIdentifier> result = new ArrayList<SecurityMechanismIdentifier>();
        for (SecurityMechanism securityMechanism :  securityMechanisms){
            if(securityMechanism.getSecurityCapabilities().containsAll(capabilitiesId)){
                result.add(new SecurityMechanismIdentifier(securityMechanism.getId()));
            }
        }
        return result;
    }
    
    private List<SecurityMechanismIdentifier> checkMultipleMetricsId (List<String> metricsId, List<SecurityMechanism> securityMechanisms){
        List<SecurityMechanismIdentifier> result = new ArrayList<SecurityMechanismIdentifier>();
        for (SecurityMechanism securityMechanism :  securityMechanisms){
            if(securityMechanism.getEnforceableMetrics().containsAll(metricsId)){
                result.add(new SecurityMechanismIdentifier(securityMechanism.getId()));
                break;
            }
            if(securityMechanism.getMonitorableMetrics().containsAll(metricsId)){
                result.add(new SecurityMechanismIdentifier(securityMechanism.getId()));
                break;
            }
        }
        return result;
    }
    
    @Override
    void persistenceRemoveSMByID(SecurityMechanismIdentifier id) {
        if (id==null)
            throw new IllegalArgumentException("SecurityMechanismIdentifier cannot be null");
        String iD = String.valueOf(id.getId());
        EntityManager em = emFactory.createEntityManager();
        SecurityMechanism securityMechanism = em.find(SecurityMechanism.class,iD);

        if (securityMechanism==null)
            throw new IllegalArgumentException("SecurityMechanismIdentifier is not valid");

        em.getTransaction().begin();
        em.remove(securityMechanism);
        em.getTransaction().commit();
        
        em.close();

    }

    @Override
    String persistenceGetSMMetadata (SecurityMechanismIdentifier id){
        if (id==null)
            throw new IllegalArgumentException("SecurityMechanism_Identifier cannot be null");
        String iD = id.getId();
        EntityManager em =  emFactory.createEntityManager();
        SecurityMechanism securityMechanism = em.find(SecurityMechanism.class,iD);

        em.close();
        if (securityMechanism==null)
            throw new IllegalArgumentException("SecurityMechanism_Identifier is not valid");
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(securityMechanism.getMetadata());
        } catch (JsonProcessingException e) {
            LoggerFactory.getLogger(EUServiceManagerAbstractImpl.class).debug("JsonProcessingException",e);
            return e.getMessage();
        }
    }


    @Override
    void persistenceUpdateSM(SecurityMechanism securityMechanism) {
        EntityManager em =  emFactory.createEntityManager();
        EntityTransaction t = em.getTransaction();
        t.begin();
        em.merge(securityMechanism);
        t.commit();
        em.close();
    }

    //SECURITY CAPABILITIES PERSISTENCE MANAGE
    @Override
    void persistenceCreateSC(SecurityCapability securityCapability) {
        EntityManager em =  emFactory.createEntityManager();
        EntityTransaction t = em.getTransaction();
        t.begin();
        em.persist(securityCapability);
        t.commit();
        em.close();
    }

    @Override
    SecurityCapability persistenceGetSCByID(SecurityCapabilityIdentifier id){
        if (id==null)
            throw new IllegalArgumentException("SecurityCapability_Identifier cannot be null");
        int iD = id.getId();
        EntityManager em =  emFactory.createEntityManager();
        SecurityCapability securityCapability = em.find(SecurityCapability.class,iD);
        em.close();
        
        if (securityCapability==null)
            throw new IllegalArgumentException("SecurityCapability_Identifier is not valid");

        return securityCapability;
    }

    @Override
    List<SecurityCapabilityIdentifier> persistenceSearchSC(){

        EntityManager em =  emFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SecurityCapability> cq = cb.createQuery(SecurityCapability.class);
        Root<SecurityCapability> rootEntry = cq.from(SecurityCapability.class);
        CriteriaQuery<SecurityCapability> all = cq.select(rootEntry);
        TypedQuery<SecurityCapability> allQuery = em.createQuery(all);

        List<SecurityCapabilityIdentifier> result = new ArrayList<SecurityCapabilityIdentifier>();

        for (SecurityCapability securityCapability :  allQuery.getResultList()){
            result.add(new SecurityCapabilityIdentifier(securityCapability.getReferenceID()));
        }

        em.close();
        return result;
    }



    @Override
    void persistenceUpdateSC(SecurityCapability securityCapability) {
        EntityManager em =  emFactory.createEntityManager();
        EntityTransaction t = em.getTransaction();
        t.begin();
        securityCapability.setUpdated(new Date());
        em.merge(securityCapability);
        t.commit();
        em.close();
    }
    
    @Override
    void persistenceRemoveSCByID(SecurityCapabilityIdentifier id) {
        if (id==null)
            throw new IllegalArgumentException("SecurityCapabilityIdentifier cannot be null");
        EntityManager em = emFactory.createEntityManager();
        SecurityCapability securityCapability = em.find(SecurityCapability.class,id.getId());

        if (securityCapability==null)
            throw new IllegalArgumentException("SecurityCapabilityIdentifier is not valid");

        em.getTransaction().begin();
        em.remove(securityCapability);
        em.getTransaction().commit();
        
        em.close();
    }

}
