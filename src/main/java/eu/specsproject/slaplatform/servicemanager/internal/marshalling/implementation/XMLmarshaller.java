/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
@author  Pasquale De Rosa p.derosa@teamandroid.it
 */

package eu.specsproject.slaplatform.servicemanager.internal.marshalling.implementation;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.slf4j.LoggerFactory;

import eu.specsproject.slaplatform.servicemanager.internal.marshalling.EntityMarshaller;
import eu.specsproject.slaplatform.servicemanager.internal.marshalling.MarshallingInterface;

public class XMLmarshaller extends EntityMarshaller{

    @Override
    public String marshal(MarshallingInterface entity, Class<MarshallingInterface> entityClass) {

        try{
             
            JAXBContext jaxbContext = JAXBContext.newInstance(entityClass);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
    
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            final StringWriter w = new StringWriter();
            jaxbMarshaller.marshal(entity, w);
            return w.toString();
        } catch (JAXBException excp) {
            LoggerFactory.getLogger(XMLentityBuilder.class).info("JAXBException", excp);
        }
        
        //manage exceptions
        return null;

    }

}
