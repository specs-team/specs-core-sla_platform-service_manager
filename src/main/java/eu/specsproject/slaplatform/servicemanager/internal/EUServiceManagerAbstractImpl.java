/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
@author  Pasquale De Rosa p.derosa@teamandroid.it
 */

package eu.specsproject.slaplatform.servicemanager.internal;

import java.io.IOException;
import java.util.List;

import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.specs.datamodel.enforcement.SecurityCapability;
import eu.specs.datamodel.enforcement.SecurityMechanism;
import eu.specsproject.slaplatform.servicemanager.ServiceManager;
import eu.specsproject.slaplatform.servicemanager.entities.SecurityCapabilityIdentifier;
import eu.specsproject.slaplatform.servicemanager.entities.SecurityMechanismDocument;
import eu.specsproject.slaplatform.servicemanager.entities.SecurityMechanismIdentifier;

public abstract class EUServiceManagerAbstractImpl implements ServiceManager{


    //SECURITY MECHANISMS PERSISTENCE FUNCTIONS
    /**
     * 
     * The method implementation must ensure that the identifier is both not null and valid. 
     * Otherwise, an {@link IllegalArgumentException} has to be thrown. 
     * 
     * @param id - SecurityMechanism identifier, not null 
     * @return the queried SecurityMechanism from persistence
     * @throws IllegalArgumentException in case id is null or SecurityMechanism is not found
     */
    abstract SecurityMechanism  persistenceGetSMByID(SecurityMechanismIdentifier id);

    /**
     * Create a SecurityMechanism in persistence.
     * 
     * @param securityMechanism - the SecurityMechanism to persist, not null
     */
    abstract void persistenceCreateSM(SecurityMechanism securityMechanism);

    /**
     * Search all SecurityMechanisms in persistence.
     * 
     */
    abstract List<SecurityMechanismIdentifier> persistenceSearchSM(List<String> capabilitiesId, List<String> metricsId, int start, int stop);

    /**
     * Create a SecurityMechanism in persistence.
     * 
     * @param securityMechanism - the SecurityMechanism to persist, not null
     */
    abstract String persistenceGetSMMetadata(SecurityMechanismIdentifier id);

    /**
     * update a Security Mechanism in persistence.
     * 
     * @param securityMechanism  - the Security Mechanism to update, not null
     */
    abstract void persistenceUpdateSM(SecurityMechanism securityMechanism);

    abstract void  persistenceRemoveSMByID(SecurityMechanismIdentifier id);

    

    //SECURITY CAPABILITIES PERSISTENCE FUNCTIONS
    /**
     * 
     * The method implementation must ensure that the identifier is both not null and valid. 
     * Otherwise, an {@link IllegalArgumentException} has to be thrown. 
     * 
     * @param id - SecurityMechanism identifier, not null 
     * @return the queried SecurityMechanism from persistence
     * @throws IllegalArgumentException in case id is null or SecurityMechanism is not found
     */
    abstract SecurityCapability  persistenceGetSCByID(SecurityCapabilityIdentifier id);

    /**
     * Create a SecurityMechanism in persistence.
     * 
     * @param securityMechanism - the SecurityMechanism to persist, not null
     */
    abstract void persistenceCreateSC(SecurityCapability securityCapability);

    /**
     * Search all SecurityMechanisms in persistence.
     * 
     */
    abstract List<SecurityCapabilityIdentifier> persistenceSearchSC();


    /**
     * update a Security Mechanism in persistence.
     * 
     * @param securityMechanism  - the Security Mechanism to update, not null
     */
    abstract void persistenceUpdateSC(SecurityCapability securityCapability);

    abstract void  persistenceRemoveSCByID(SecurityCapabilityIdentifier id);



    //SECURITY MECHANISMS FUNCTIONS

    @Override
    public SecurityMechanismIdentifier createSM(SecurityMechanismDocument securityMechanism) throws JsonParseException, JsonMappingException, IOException {

        if (securityMechanism==null)
            throw new IllegalArgumentException("The SecurityMechanism_Document cannot be null");

        //create SecurityMechanism
        SecurityMechanism internalSecurityMechanism = new SecurityMechanism();
        LoggerFactory.getLogger(EUServiceManagerAbstractImpl.class).debug("json: "+securityMechanism.getSecurityMechanismJsonDocument());
        ObjectMapper mapper = new ObjectMapper();
            internalSecurityMechanism = mapper.readValue(securityMechanism.getSecurityMechanismJsonDocument(), SecurityMechanism.class);
            persistenceCreateSM(internalSecurityMechanism);

            return new  SecurityMechanismIdentifier(internalSecurityMechanism.getId());
//        internalSecurityMechanism = new Gson().fromJson(securityMechanism.getSecurityMechanismJsonDocument(), SecurityMechanism.class);

        
    }

    @Override
    public SecurityMechanism retrieveSM(SecurityMechanismIdentifier id) {

        return  persistenceGetSMByID(id);   
    }

    @Override
    public List<SecurityMechanismIdentifier> searchSMs(List<String> capabilitiesId, List<String> metricsId, int start, int stop){
        return persistenceSearchSM(capabilitiesId, metricsId, start, stop);
    }
    
    @Override
    public String retrieveSMMetadata(SecurityMechanismIdentifier id){
        return persistenceGetSMMetadata(id);
    }



    @Override
    public void updateSM(SecurityMechanismIdentifier id,
            SecurityMechanismDocument securityMechanism){

        SecurityMechanism intsm = persistenceGetSMByID(id);

        if (securityMechanism==null)
            throw new IllegalArgumentException("SecurityMechanism cannot be null");

        LoggerFactory.getLogger(EUServiceManagerAbstractImpl.class).debug("json: "+securityMechanism.getSecurityMechanismJsonDocument());

        ObjectMapper mapper = new ObjectMapper();
        try {
            intsm = mapper.readValue(securityMechanism.getSecurityMechanismJsonDocument(), SecurityMechanism.class);
            persistenceUpdateSM(intsm);
        } catch (IOException e) {
            LoggerFactory.getLogger(EUServiceManagerAbstractImpl.class).debug("IOException",e);
        }

    }
    
    @Override
    public SecurityMechanismIdentifier removeSM(SecurityMechanismIdentifier id){
        persistenceRemoveSMByID(id);
        return id;
    }

    @Override
    public void annotateSM(SecurityMechanismIdentifier id, Object object) {
    }

    @Override
    public void getAnnotationsSM(SecurityMechanismIdentifier id) {
    }

    @Override
    public void updateAnnotationSM(SecurityMechanismIdentifier id, String annId,
            Object object) {
    }

    //SECURITY CAPABILITIES FUNCTIONS
    @Override
    public SecurityCapabilityIdentifier createSC(String securityCapability) {

        if (securityCapability==null)
            throw new IllegalArgumentException("The SecurityCapability_Document cannot be null");

        //create SecurityMechanism
        SecurityCapability internalSecurityCapability = new SecurityCapability();

        LoggerFactory.getLogger(EUServiceManagerAbstractImpl.class).debug("capability: "+securityCapability);

        internalSecurityCapability.setXMLdescription(securityCapability);

        persistenceCreateSC(internalSecurityCapability);

        return new  SecurityCapabilityIdentifier(internalSecurityCapability.getReferenceID());
    }

    @Override
    public SecurityCapability retrieveSC(SecurityCapabilityIdentifier id){
        return  persistenceGetSCByID(id);   
    }

    @Override
    public List<SecurityCapabilityIdentifier> searchSCs(){
        return persistenceSearchSC();
    }


    @Override
    public void updateSC(SecurityCapabilityIdentifier id,
            String securityCapability){

        SecurityCapability intsm = persistenceGetSCByID(id);

        if (securityCapability==null)
            throw new IllegalArgumentException("SecurityCapability cannot be null");

        intsm.setXMLdescription(securityCapability);
        
        persistenceUpdateSC(intsm);
    }
    
    @Override
    public SecurityCapabilityIdentifier removeSC(SecurityCapabilityIdentifier id){
        persistenceRemoveSCByID(id);
        return id;
    }

    
    @Override
    public void annotateSC(SecurityCapabilityIdentifier id, Object object) {
    }

    @Override
    public void getAnnotationsSC(SecurityCapabilityIdentifier id) {
    }

    @Override
    public void updateAnnotationSC(SecurityCapabilityIdentifier id, String annId,
            Object object) {
    }

}
