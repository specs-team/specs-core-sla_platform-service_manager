/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
@author  Pasquale De Rosa p.derosa@teamandroid.it
 */

package eu.specsproject.slaplatform.servicemanager;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import eu.specs.datamodel.enforcement.SecurityCapability;
import eu.specs.datamodel.enforcement.SecurityMechanism;
import eu.specsproject.slaplatform.servicemanager.entities.SecurityCapabilityIdentifier;
import eu.specsproject.slaplatform.servicemanager.entities.SecurityMechanismDocument;
import eu.specsproject.slaplatform.servicemanager.entities.SecurityMechanismIdentifier;


/**
 *  The Service_Manager permits to manage Security Mechanisms and the Capabilites and Metrics
 *  associated.
 * 
 * @author Pasquale De Rosa SPECS  CeRICT
 * 
 */
public interface ServiceManager {

    //************SECURITY MECHANISMS FUNCTIONS*************//

    /**
     * Create and persist a Security Capability into the SLA Platform.
     * The Security Capability has to be not null, well formed and valid (see {@link SecurityMechanismDocument}), otherwise an {@link IllegalArgumentException} will be thrown.
     * 
     * @param securityCapability  A valid and well formed Security Capability to persist.
     * @return an opaque object that identifies the created Security Capability, intended to be used to perform further operation on it.
     * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
     */
    public SecurityMechanismIdentifier createSM(SecurityMechanismDocument securityMechanism) throws JsonParseException, JsonMappingException, IOException;


    /**
     * Retrieve a Security Mechanism from the persistence.
     * An {@link IllegalArgumentException} will be thrown in case of a null or not valid identifier is used as argument.
     * 
     * @param id  a valid and not null SecurityMechanism identifier.
     * @return the persisted Security Mechanism
     * @throws IllegalArgumentException in case of Security Mechanism argument is null, not valid nor well formed.
     */
    public SecurityMechanism retrieveSM(SecurityMechanismIdentifier id);

    /**
     * Retrieve all Security Mechanisms from the persistence.
     * 
     * @return all the persisted Security Mechanism
     * @throws IllegalArgumentException in case of Security Mechanism argument is null, not valid nor well formed.
     */
    public List<SecurityMechanismIdentifier> searchSMs(List<String> capabilitiesId, List<String> metricsId, int start, int stop);

    /**
     * Edits a Security Mechanisms in the persistence.
     * An {@link IllegalArgumentException} will be thrown in case of a null or not valid identifier is used as argument.
     * 
     * @param id  a valid and not null SecurityMechanism identifier.
     * @param securityMechanism  a valid and not null SecurityMechanism_Document.
     * @throws IllegalArgumentException in case of SecurityMechanism_Document argument is null, not valid nor well formed.
     */
    public void updateSM(SecurityMechanismIdentifier id,
            SecurityMechanismDocument securityMechanism);

    /**
     * Retrieve a Security Mechanism from the persistence.
     * An {@link IllegalArgumentException} will be thrown in case of a null or not valid identifier is used as argument.
     * 
     * @param id  a valid and not null SecurityMechanism identifier.
     * @return the persisted Security Mechanism
     * @throws IllegalArgumentException in case of Security Mechanism argument is null, not valid nor well formed.
     */
    public String retrieveSMMetadata(SecurityMechanismIdentifier id);
    
    public SecurityMechanismIdentifier removeSM(SecurityMechanismIdentifier id);



    public void annotateSM(SecurityMechanismIdentifier id, Object object);


    public void getAnnotationsSM(SecurityMechanismIdentifier id);


    public void updateAnnotationSM(SecurityMechanismIdentifier id, String annId,
            Object object);


    //************SECURITY CAPABILITY FUNCTIONS*************//

    /**
     * Create and persist a Security Capability into the SLA Platform.
     * The Security Capability has to be not null, well formed and valid (see {@link SecurityMechanismDocument}), otherwise an {@link IllegalArgumentException} will be thrown.
     * 
     * @param securityCapability  A valid and well formed Security Capability to persist.
     * @return an opaque object that identifies the created Security Capability, intended to be used to perform further operation on it.
     * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
     */
    public SecurityCapabilityIdentifier createSC(String securityCapability);


    /**
     * Retrieve a Security Mechanism from the persistence.
     * An {@link IllegalArgumentException} will be thrown in case of a null or not valid identifier is used as argument.
     * 
     * @param id  a valid and not null SecurityMechanism identifier.
     * @return the persisted Security Mechanism
     * @throws IllegalArgumentException in case of Security Mechanism argument is null, not valid nor well formed.
     */
    public SecurityCapability retrieveSC(SecurityCapabilityIdentifier id);

    /**
     * Retrieve all Security Mechanisms from the persistence.
     * 
     * @return all the persisted Security Mechanism
     * @throws IllegalArgumentException in case of Security Mechanism argument is null, not valid nor well formed.
     */
    public List<SecurityCapabilityIdentifier> searchSCs();

    /**
     * Edits a Security Mechanisms in the persistence.
     * An {@link IllegalArgumentException} will be thrown in case of a null or not valid identifier is used as argument.
     * 
     * @param id  a valid and not null SecurityMechanism identifier.
     * @param securityMechanism  a valid and not null SecurityMechanism_Document.
     * @throws IllegalArgumentException in case of SecurityMechanism_Document argument is null, not valid nor well formed.
     */
    public void updateSC(SecurityCapabilityIdentifier id,
            String securityCapability);
    
    public SecurityCapabilityIdentifier removeSC(SecurityCapabilityIdentifier id);


    public void annotateSC(SecurityCapabilityIdentifier id, Object object);


    public void getAnnotationsSC(SecurityCapabilityIdentifier id);


    public void updateAnnotationSC(SecurityCapabilityIdentifier id, String annId,
            Object object);

}
