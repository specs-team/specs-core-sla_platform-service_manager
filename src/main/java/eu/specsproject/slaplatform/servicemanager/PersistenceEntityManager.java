package eu.specsproject.slaplatform.servicemanager;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceEntityManager {

	private static final PersistenceEntityManager singleton = new PersistenceEntityManager();

	protected EntityManagerFactory emf;

	public static PersistenceEntityManager getInstance() {

		return singleton;
	}

	private PersistenceEntityManager() {
	}

	public EntityManagerFactory getEntityManagerFactory(boolean isTest) {

		if (emf == null)
			createEntityManagerFactory(isTest);
		return emf;
	}

	public void closeEntityManagerFactory() {

		if (emf != null) {
			emf.close();
			emf = null;
		}
	}

	protected void createEntityManagerFactory(boolean isTest) {

		if(isTest){
			this.emf = Persistence.createEntityManagerFactory("test_unit");
		}else{
			this.emf = Persistence.createEntityManagerFactory("mongodb");
		}
	}
}
