package eu.specsproject.slaplatform.servicemanager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.specs.datamodel.enforcement.SecurityMechanism;

public class Main {

EntityManagerFactory factory;
    
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Main test = new Main();
        test.factory = Persistence.createEntityManagerFactory("mongodb");
        test.testPersist();
        //test.testFind();
    }
    
    public void testPersist(){    
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        ObjectMapper mapper = new ObjectMapper();
        //JSON from String to Object
        SecurityMechanism sm;
        try {
            String jsonInString = readFile("src/main/resources/mechanism-webpool.json");
            sm = mapper.readValue(jsonInString, SecurityMechanism.class);
            em.persist(sm);

            em.getTransaction().commit();
            System.out.println("Id assegnato: "+sm.getMonitorableMetrics().get(0));
            em.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        
    }
    
    /*
    public void testFind() {
        System.out.println("\nTesting find() by Id.\n");
        EntityManager em = factory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SLA> cq = cb.createQuery(SLA.class);
        Root<SLA> rootEntry = cq.from(SLA.class);
        CriteriaQuery<SLA> all = cq.select(rootEntry);
        TypedQuery<SLA> allQuery = em.createQuery(all);
        System.out.println("Found SLAS:" + allQuery.getResultList().size());
        for (SLA sla :  allQuery.getResultList()){
            if(sla.getDoc() != null){
                System.out.println("Sla document: "+sla.getDoc().getSlaXmlDocument());
            }
        }
        em.close();
    }*/
    
    private static String readFile(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }


}
