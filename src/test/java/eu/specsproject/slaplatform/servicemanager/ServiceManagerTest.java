package eu.specsproject.slaplatform.servicemanager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import eu.specs.datamodel.enforcement.Measurement;
import eu.specs.datamodel.enforcement.SecurityCapability;
import eu.specs.datamodel.enforcement.SecurityMechanism;
import eu.specsproject.slaplatform.servicemanager.entities.SecurityCapabilityDocument;
import eu.specsproject.slaplatform.servicemanager.entities.SecurityCapabilityIdentifier;
import eu.specsproject.slaplatform.servicemanager.entities.SecurityMechanismDocument;
import eu.specsproject.slaplatform.servicemanager.entities.SecurityMechanismIdentifier;
import eu.specsproject.slaplatform.servicemanager.internal.EUServiceManagerSQLJPA;
import eu.specsproject.slaplatform.servicemanager.internal.marshalling.MarshallingInterface;
import eu.specsproject.slaplatform.servicemanager.internal.marshalling.implementation.JSONentityBuilder;
import eu.specsproject.slaplatform.servicemanager.internal.marshalling.implementation.JSONmarshaller;
import eu.specsproject.slaplatform.servicemanager.internal.marshalling.implementation.XMLentityBuilder;
import eu.specsproject.slaplatform.servicemanager.internal.marshalling.implementation.XMLmarshaller;

public class ServiceManagerTest {

	private static ServiceManager sm;
	private SecurityMechanismIdentifier id,id2,id3;
	private SecurityCapabilityIdentifier ic;
	private SecurityMechanismDocument securitymechanism = null;
	
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("setUpBeforeClass Called");
        sm = (EUServiceManagerSQLJPA) ServiceManagerFactory.getServiceManagerInstance(true);
        Assert.assertNotNull(sm);
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        //do nothing
    }

    @Before
    public void setUp() throws Exception {
        System.out.println("setUp Called");
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("tearDown Called");
    }
    
    @Test
    public final void testSMF(){     
        
        Assert.assertNotNull(ServiceManagerFactory.getServiceManagerInstance(false));

    	Assert.assertNotNull(ServiceManagerFactory.getServiceManagerInstance(true));
    }
    
    @Test
    public final void createSMDTest() throws IOException {
    	SecurityMechanismDocument smd = new SecurityMechanismDocument(readFile("src/test/resources/mechanism-webpool.json"));
    	id = sm.createSM(smd);
    	
    	Assert.assertNotNull(smd);
    	
    	SecurityMechanismDocument smd2 = new SecurityMechanismDocument(readFile("src/test/resources/mechanism-sva.json"));
    	id2 = sm.createSM(smd2);
    	
    	Assert.assertNotNull(smd2);
    	
    	SecurityMechanismDocument smd3 = new SecurityMechanismDocument(readFile("src/test/resources/mechanism-sva3.json"));
    	id3 = sm.createSM(smd3);
    	
    	Assert.assertNotNull(smd3);
    	
    	SecurityMechanism secm = sm.retrieveSM(id2);
    	Assert.assertEquals(secm.getId(), id2.toString());
    	
    	Assert.assertNotNull(secm.getName());
    	Assert.assertNotNull(secm.getDescription());
    	
    	List<Measurement> measures = secm.getMeasurements();
    	Assert.assertNotNull(measures);
    	
    	Assert.assertNotNull(id);
    	Assert.assertNotNull(id2);
    	Assert.assertNotNull(id3);
    	
    	List<SecurityMechanismIdentifier> smi = new ArrayList<SecurityMechanismIdentifier>();
    	smi.add(id);
    	smi.add(id2);
    	smi.add(id3);
    	
    	Assert.assertNotNull(smi);
    	Assert.assertEquals(smi.get(0), id);
    	
    	String metadata = sm.retrieveSMMetadata(id2);
    	Assert.assertNotNull(metadata);
    	
    	sm.updateSM(id2, smd3);
    	
    	sm.removeSM(id2);
    	
    	smd.setSecurityMechanismJsonDocument("security-mechanism-doc");
    	Assert.assertEquals("security-mechanism-doc", smd.getSecurityMechanismJsonDocument());
    	
    	SecurityMechanismDocument smd1 = new SecurityMechanismDocument();
    	Assert.assertNotNull(smd1.toString());
    	
    	SecurityMechanismIdentifier smi1 = new SecurityMechanismIdentifier();
    	Assert.assertNotNull(smi1);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public final void exceptionCreateSmTest(){
    	try {
			sm.createSM(null);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void exceptionUpdateSmTest(){
    	
    	sm.updateSM(id, securitymechanism);
    }
    
    @Test
    public final void annotateSMTest(){
    	Object o = null;
    	sm.annotateSM(id, o);
    	Assert.assertTrue(true);
    }
    
    @Test
    public final void getAnnotationsSMTest(){
    	sm.getAnnotationsSM(id);
    	Assert.assertTrue(true);
    }
    
    @Test
    public final void updateAnnotationsSMTest(){
    	String annId = "This-is-annotation";
    	Object o = null;
    	sm.updateAnnotationSM(id, annId, o);
    	Assert.assertTrue(true);
    }
    
    @Test
    public final void searchSMsTest() throws FileNotFoundException{
    	
    	List<SecurityMechanismIdentifier> lsmi = sm.searchSMs(null, null, 0, 10);
    	Assert.assertEquals(lsmi.size(), 0);
    }
    
    @Test
    public final void searchSMsTestC() throws FileNotFoundException{
        List<String> list = new ArrayList<String>();
        list.add("software_vulnerability");
    	List<SecurityMechanismIdentifier> lsmi = sm.searchSMs(list, null, 0, 10);
    	Assert.assertNotNull(lsmi);
    }
    
    @Test
    public final void searchSMsTestCs() throws FileNotFoundException{
    	List<String> capabilities = new ArrayList<String>();
    	capabilities.add("software_vulnerability");
    	List<SecurityMechanismIdentifier> lsmi = sm.searchSMs(null, capabilities , 0, 10);
    	Assert.assertNotNull(lsmi);
    }
    
    @Test
    public final void searchSMsTestCCs() throws FileNotFoundException{
        List<String> list = new ArrayList<String>();
        list.add("software_vulnerability");
    	List<String> capabilities = new ArrayList<String>();
    	capabilities.add("software_vulnerability");
    	List<SecurityMechanismIdentifier> lsmi = sm.searchSMs(list, capabilities , 0, 10);
    	Assert.assertNotNull(lsmi);
    }
    
//    @Test
//    public final void searchSMsTestMMs(){
//    	List<String> metrics = new ArrayList<String>();
////    	metrics.add("list_update_frequency_m14");
////    	metrics.add("extended_scan_frequency_m22");
////    	metrics.add("up_report_frequency_m23");
//    	metrics.add("level_of_redundancy_m1");
//        metrics.add("level_of_diversity_m2");
//    	List<SecurityMechanismIdentifier> lsmi = sm.searchSMs(null, null , "level_of_redundancy_m1", metrics);
//    	Assert.assertNotNull(lsmi);
//    }
//    
//    @Test
//    public final void searchSMsTestAll() throws FileNotFoundException{
//    	List<String> metrics = new ArrayList<String>();
//    	metrics.add("level_of_redundancy_m1");
//    	metrics.add("level_of_diversity_m2");
//        List<String> capabilities = new ArrayList<String>();
//    	capabilities.add("software_vulnerability");
//    	List<SecurityMechanismIdentifier> lsmi = sm.searchSMs("web_resilience", capabilities , "level_of_redundancy_m1", metrics);
//    	Assert.assertNotNull(lsmi);
//    }
    
    @Test
    public final void createSCTest() throws IOException{
    	SecurityMechanismDocument smd2 = new SecurityMechanismDocument(readFile("src/test/resources/mechanism-sva.json"));
    	id2 = sm.createSM(smd2);
    	
    	List<String> capabilities = (sm.retrieveSM(id2)).getSecurityCapabilities();
    	ic = sm.createSC(capabilities.get(0));
    	Assert.assertNotNull(ic);
    	
    	SecurityCapability sc = sm.retrieveSC(ic);
    	Assert.assertNotNull(sc);
    	
    	List<SecurityCapabilityIdentifier> lsci = sm.searchSCs();
    	Assert.assertNotEquals(lsci.size(), 0);
    	
    	sm.updateSC(ic, "lho-cambiata-io");
    	SecurityCapability scmod = sm.retrieveSC(ic);
    	Assert.assertNotEquals(sc.getXMLdescription(), scmod.getXMLdescription());
    	
    	SecurityCapabilityIdentifier icdel = sm.removeSC(ic);
    	Assert.assertNotNull(icdel);
    	
    	String cap = "security-capability-doc";
    	SecurityCapabilityDocument scd = new SecurityCapabilityDocument();
    	scd.setSecurityCapabilityXmlDocument(cap);
    	Assert.assertEquals(cap, scd.getSecurityCapabilityXmlDocument());
    	
    	SecurityCapabilityIdentifier sci = new SecurityCapabilityIdentifier();
    	Assert.assertNotNull(sci);
    	
    	Assert.assertNotNull(sci.toString());
    	
    	SecurityCapabilityDocument scd1 = new SecurityCapabilityDocument("mechanism-sva2.json");
    	Assert.assertNotNull(scd1);
    	
    }
    
    @Test(expected=IllegalArgumentException.class)
    public final void exceptionCreateScTest(){
    	sm.createSC(null);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public final void exceptionUpdateScTest(){
    	sm.updateSC(ic,"");
    }
    
    @Test
    public final void annotateSCTest(){
    	Object o = null;
    	sm.annotateSC(ic, o);
    	Assert.assertTrue(true);
    }
    
    @Test
    public final void getAnnotationsSCTest(){
    	sm.getAnnotationsSC(ic);
    	Assert.assertTrue(true);
    }
    
    @Test
    public final void updateAnnotationsSCTest(){
    	String annId = "This-is-annotation";
    	Object o = null;
    	sm.updateAnnotationSC(ic, annId, o);
    	Assert.assertTrue(true);
    }
    
    
    
    @Test
    public void unMarshalTest(){
        JSONentityBuilder builder = new JSONentityBuilder ();
        MarshallingInterface inf = builder.unmarshal("", MarshallingInterface.class);
        Assert.assertNotNull(inf);
    }
    
    
    @Test
    public void marshalTest(){
        JSONmarshaller marshaller = new JSONmarshaller();
        MarshallingInterface inf = null;
        String marshString = marshaller.marshal(inf, MarshallingInterface.class);
        Assert.assertNotNull(marshString);
        
    }
    
    @Test
    public void XMLunMarshalTest(){
        XMLentityBuilder builder = new XMLentityBuilder();
        MarshallingInterface inf = builder.unmarshal("XMLmarshallerTest", MarshallingInterface.class);
        Assert.assertNull(inf);
    }
    
    @Test
    public void XMLmarshalTest(){
        XMLmarshaller marshaller = new XMLmarshaller();
        MarshallingInterface inf = null;
        String marshString = marshaller.marshal(inf, MarshallingInterface.class);
        Assert.assertNull(marshString);
    }
    
    
    private String readFile(String fileName) throws IOException {
	    BufferedReader br = new BufferedReader(new FileReader(fileName));
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            sb.append("\n");
	            line = br.readLine();
	        }
	        return sb.toString();
	    } finally {
	        br.close();
	    }
	}
}